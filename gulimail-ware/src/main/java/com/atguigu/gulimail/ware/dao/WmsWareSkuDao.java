package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.WmsWareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:36:33
 */
@Mapper
public interface WmsWareSkuDao extends BaseMapper<WmsWareSkuEntity> {
	
}
