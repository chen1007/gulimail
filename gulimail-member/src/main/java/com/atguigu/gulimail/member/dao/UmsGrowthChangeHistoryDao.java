package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsGrowthChangeHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 成长值变化历史记录
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:16
 */
@Mapper
public interface UmsGrowthChangeHistoryDao extends BaseMapper<UmsGrowthChangeHistoryEntity> {
	
}
