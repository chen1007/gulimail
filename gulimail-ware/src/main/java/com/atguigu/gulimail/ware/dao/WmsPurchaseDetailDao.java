package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.WmsPurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:36:35
 */
@Mapper
public interface WmsPurchaseDetailDao extends BaseMapper<WmsPurchaseDetailEntity> {
	
}
