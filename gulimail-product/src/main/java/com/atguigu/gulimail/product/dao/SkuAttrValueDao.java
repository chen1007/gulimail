package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SkuAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku销售属性&值
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-08-03 11:49:21
 */
@Mapper
public interface SkuAttrValueDao extends BaseMapper<SkuAttrValueEntity> {
	
}
