package com.atguigu.gulimail.coupon.controller;

import com.atguigu.gulimail.coupon.feign.ProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @ClassName TestController
 * @Description 服务消费者测试
 * @Author chen
 * @Date 2020/8/3 8:28
 */
@Controller
@RequestMapping("/consumer")
public class TestController {

    @Autowired
    private ProviderClient providerClient;

    @RequestMapping("/hello-consumer")
    @ResponseBody
    public String hello(){
        String result=providerClient.helloWorld();
        return result;
    }
}
