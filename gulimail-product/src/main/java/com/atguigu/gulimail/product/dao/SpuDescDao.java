package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SpuDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-08-03 11:49:20
 */
@Mapper
public interface SpuDescDao extends BaseMapper<SpuDescEntity> {
	
}
