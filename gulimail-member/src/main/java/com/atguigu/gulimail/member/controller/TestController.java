package com.atguigu.gulimail.member.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimail.member.fegin.ProviderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @ClassName TestController
 * @Description fegin 服务提供者
 * @Author chen
 * @Date 2020/7/31 15:33
 */
@Controller
public class TestController {

    @RequestMapping(value = "hello",method = RequestMethod.GET)
    @ResponseBody
    public R feign(){
        return R.ok("测试fegin成功");
    };
}
