package com.atguigu.gulimail.product.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimail.product.entity.CategoryEntity;
import com.atguigu.gulimail.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @ClassName TestController
 * @Description 测试类
 * @Author chen
 * @Date 2020/8/3 13:48
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("treeList")
    @ResponseBody
    public R treeList(){
        List<CategoryEntity> treeList=categoryService.treeList();
        return  R.ok().put( "treeList",treeList );
    }
}
