package com.atguigu.gulimail.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimail.coupon.entity.SmsSeckillSkuEntity;

import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 15:57:09
 */
public interface SmsSeckillSkuService extends IService<SmsSeckillSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

