package com.atguigu.gulimail.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimail.member.entity.UmsMemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:16
 */
public interface UmsMemberService extends IService<UmsMemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

