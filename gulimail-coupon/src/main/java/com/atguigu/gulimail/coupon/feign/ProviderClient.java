package com.atguigu.gulimail.coupon.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 服务消费者
 * @FeignClient("gulimail-member") 服务提供者的spring.application.name
 */
@FeignClient("gulimail-member")
public interface ProviderClient {

    @RequestMapping(method = RequestMethod.GET,value = "hello")
    String helloWorld();
}
