package com.atguigu.gulimail.coupon.dao;

import com.atguigu.gulimail.coupon.entity.SmsSkuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 15:57:08
 */
@Mapper
public interface SmsSkuBoundsDao extends BaseMapper<SmsSkuBoundsEntity> {
	
}
