package com.atguigu.gulimail.member.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName ProviderClient
 * @Description 服务提供者
 * @Author chen
 * @Date 2020/7/31 16:48
 */
@FeignClient("gulimail-member")
public interface ProviderClient {

    @RequestMapping(value = "/hello")
    String helloWorld();
}
