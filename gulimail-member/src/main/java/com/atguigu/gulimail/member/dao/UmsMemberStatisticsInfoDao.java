package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:13
 */
@Mapper
public interface UmsMemberStatisticsInfoDao extends BaseMapper<UmsMemberStatisticsInfoEntity> {
	
}
