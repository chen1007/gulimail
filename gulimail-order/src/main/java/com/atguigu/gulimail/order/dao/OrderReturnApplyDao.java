package com.atguigu.gulimail.order.dao;

import com.atguigu.gulimail.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 14:38:34
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
