package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberReceiveAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收货地址
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:14
 */
@Mapper
public interface UmsMemberReceiveAddressDao extends BaseMapper<UmsMemberReceiveAddressEntity> {
	
}
