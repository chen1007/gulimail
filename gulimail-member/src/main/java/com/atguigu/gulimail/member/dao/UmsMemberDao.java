package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:16
 */
@Mapper
public interface UmsMemberDao extends BaseMapper<UmsMemberEntity> {
	
}
