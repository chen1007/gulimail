package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsIntegrationChangeHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 积分变化历史记录
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:16
 */
@Mapper
public interface UmsIntegrationChangeHistoryDao extends BaseMapper<UmsIntegrationChangeHistoryEntity> {
	
}
