package com.atguigu.gulimail.coupon.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimail.coupon.dao.SmsSeckillSkuDao;
import com.atguigu.gulimail.coupon.entity.SmsSeckillSkuEntity;
import com.atguigu.gulimail.coupon.service.SmsSeckillSkuService;


@Service("smsSeckillSkuService")
public class SmsSeckillSkuServiceImpl extends ServiceImpl<SmsSeckillSkuDao, SmsSeckillSkuEntity> implements SmsSeckillSkuService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SmsSeckillSkuEntity> page = this.page(
                new Query<SmsSeckillSkuEntity>().getPage(params),
                new QueryWrapper<SmsSeckillSkuEntity>()
        );

        return new PageUtils(page);
    }

}