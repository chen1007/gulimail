package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-08-03 11:49:22
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
