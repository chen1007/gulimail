package com.atguigu.gulimail.coupon.controller;

import java.util.Arrays;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atguigu.gulimail.coupon.entity.SmsSeckillSkuEntity;
import com.atguigu.gulimail.coupon.service.SmsSeckillSkuService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 秒杀活动商品关联
 *
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 15:57:09
 */
@RestController
@RequestMapping("coupon/smsseckillsku")
public class SmsSeckillSkuController {
    @Autowired
    private SmsSeckillSkuService smsSeckillSkuService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = smsSeckillSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		SmsSeckillSkuEntity smsSeckillSku = smsSeckillSkuService.getById(id);

        return R.ok().put("smsSeckillSku", smsSeckillSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SmsSeckillSkuEntity smsSeckillSku){
		smsSeckillSkuService.save(smsSeckillSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SmsSeckillSkuEntity smsSeckillSku){
		smsSeckillSkuService.updateById(smsSeckillSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		smsSeckillSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
