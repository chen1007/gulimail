package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberLoginLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员登录记录
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:14
 */
@Mapper
public interface UmsMemberLoginLogDao extends BaseMapper<UmsMemberLoginLogEntity> {
	
}
