package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:15
 */
@Mapper
public interface UmsMemberCollectSpuDao extends BaseMapper<UmsMemberCollectSpuEntity> {
	
}
