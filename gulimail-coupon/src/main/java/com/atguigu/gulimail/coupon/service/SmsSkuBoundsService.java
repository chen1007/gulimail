package com.atguigu.gulimail.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimail.coupon.entity.SmsSkuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 15:57:08
 */
public interface SmsSkuBoundsService extends IService<SmsSkuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

