package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.WmsPurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:36:33
 */
@Mapper
public interface WmsPurchaseDao extends BaseMapper<WmsPurchaseEntity> {
	
}
