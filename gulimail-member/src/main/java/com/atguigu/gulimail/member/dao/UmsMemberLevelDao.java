package com.atguigu.gulimail.member.dao;

import com.atguigu.gulimail.member.entity.UmsMemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author fangchen
 * @email 695313717@qq.com
 * @date 2020-07-29 16:14:15
 */
@Mapper
public interface UmsMemberLevelDao extends BaseMapper<UmsMemberLevelEntity> {
	
}
